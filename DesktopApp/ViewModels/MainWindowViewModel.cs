﻿namespace DatabaseSubsettingTool.DesktopApp.ViewModels
{
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows.Input;

    using DatabaseSubsettingTool.Core.Enum.Workload;
    using DatabaseSubsettingTool.Core.Model.Workload;
    using DatabaseSubsettingTool.DesktopApp.Commands;
    using DatabaseSubsettingTool.DesktopApp.Extension;

    public class MainWindowViewModel : AbstractViewModel
    {
        #region Queries

        private ObservableCollection<SqlQuery> queries = new ObservableCollection<SqlQuery>();

        public ObservableCollection<SqlQuery> Queries
        {
            get
            {
                return this.queries;
            }

            set
            {
                if (object.Equals(value, this.queries))
                {
                    return;
                }

                this.queries = value;
                this.OnPropertyChanged(nameof(this.Queries));
                this.OnPropertyChanged(nameof(this.InvalidQueryCount));
            }
        }

        public int InvalidQueryCount
        {
            get
            {
                return this.Queries.Count(x => !x.Valid);
            }
        }

        #endregion Queries


        #region Remove invalid queries button

        private RelayCommand removeInvalidQueriesCommand;

        public ICommand RemoveInvalidQueriesCommand
        {
            get
            {
                if (this.removeInvalidQueriesCommand == null)
                {
                    this.removeInvalidQueriesCommand = new RelayCommand(
                        this.RemoveInvalidQueriesCommandExecute,
                        this.RemoveInvalidQueriesCommandCanExecute
                    );
                }

                return this.removeInvalidQueriesCommand;
            }
        }

        public void RemoveInvalidQueriesCommandExecute(object obj)
        {
            this.Queries.RemoveAll(x => !x.Valid);
            this.OnPropertyChanged(nameof(this.InvalidQueryCount));
        }

        public bool RemoveInvalidQueriesCommandCanExecute()
        {
            return this.Queries != null && !this.Queries.All(x => x.Valid);
        }

        #endregion Remove invalid queries button


        #region Is subsetting btn enabled

        private bool subsettingBtnEnabled;

        public bool SubsettingBtnEnabled
        {
            get
            {
                return this.subsettingBtnEnabled && this.Queries.Count > 0;
            }

            set
            {
                this.subsettingBtnEnabled = value;
                this.OnPropertyChanged(nameof(this.SubsettingBtnEnabled));
            }
        }

        #endregion Is subsetting btn enabled


        #region Is testing btn enabled

        private bool testingBtnEnabled;

        public bool TestingBtnEnabled
        {
            get
            {
                return this.testingBtnEnabled && this.Queries.Count > 0;
            }

            set
            {
                this.testingBtnEnabled = value;
                this.OnPropertyChanged(nameof(this.TestingBtnEnabled));
            }
        }

        #endregion Is testing btn enabled


        #region Destination referential integrity

        private bool? isDestinationReferentianIntegrityMaintained = null;

        public bool? IsDestinationReferentianIntegrityMaintained
        {
            get
            {
                return this.isDestinationReferentianIntegrityMaintained;
            }

            set
            {
                this.isDestinationReferentianIntegrityMaintained = value;
                this.OnPropertyChanged(nameof(this.IsDestinationReferentianIntegrityMaintained));
            }
        }

        #endregion Destination referential integrity


        #region Total execution time in miliseconds

        private int? totalExecutionTime = null;

        public int? TotalExecutionTime
        {
            get
            {
                return this.totalExecutionTime;
            }

            set
            {
                this.totalExecutionTime = value;
                this.OnPropertyChanged(nameof(this.TotalExecutionTime));
            }
        }

        #endregion Total execution time in miliseconds


        #region Cloning execution time in miliseconds

        private int? cloningExecutionTime = null;

        public int? CloningExecutionTime
        {
            get
            {
                return this.cloningExecutionTime;
            }

            set
            {
                this.cloningExecutionTime = value;
                this.OnPropertyChanged(nameof(this.CloningExecutionTime));
            }
        }

        #endregion Cloning execution time in miliseconds


        #region Subsetting execution time in miliseconds

        private int? subsettingExecutionTime = null;

        public int? SubsettingExecutionTime
        {
            get
            {
                return this.subsettingExecutionTime;
            }

            set
            {
                this.subsettingExecutionTime = value;
                this.OnPropertyChanged(nameof(this.SubsettingExecutionTime));
            }
        }

        #endregion Subsetting execution time in miliseconds


        #region Copying dependencies execution time in miliseconds

        private int? copyingDependenciesExecutionTime = null;

        public int? CopyingDependenciesExecutionTime
        {
            get
            {
                return this.copyingDependenciesExecutionTime;
            }

            set
            {
                this.copyingDependenciesExecutionTime = value;
                this.OnPropertyChanged(nameof(this.CopyingDependenciesExecutionTime));
            }
        }

        #endregion Copying dependencies execution time in miliseconds


        #region Tables

        private ObservableCollection<Table> tables = new ObservableCollection<Table>();

        public ObservableCollection<Table> Tables
        {
            get
            {
                return this.tables;
            }

            set
            {
                if (object.Equals(value, this.tables))
                {
                    return;
                }

                this.tables = value;
                this.OnPropertyChanged(nameof(this.Tables));
            }
        }

        #endregion Tables


        #region Reset view model state

        public void ResetViewModelState()
        {
            foreach (SqlQuery query in this.Queries)
            {
                query.Processed = null;
                query.IdenticalResultCount = null;
                query.IdenticalExecutionPlan = null;
                query.ExecutionTime = null;
                query.ErrorMessages.Remove(SqlQueryErrorMessageType.IdenticalExecutionPlan);
                query.ErrorMessages.Remove(SqlQueryErrorMessageType.IdenticalResultCount);
            }

            this.IsDestinationReferentianIntegrityMaintained = null;
            this.TotalExecutionTime = null;
            this.CloningExecutionTime = null;
            this.SubsettingExecutionTime = null;
            this.CopyingDependenciesExecutionTime = null;
            this.Tables = new ObservableCollection<Table>();
        }

        #endregion Reset view model state
    }
}

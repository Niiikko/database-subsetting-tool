﻿namespace DatabaseSubsettingTool.DesktopApp
{
    using System;
    using System.Runtime.InteropServices;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Interop;

    public partial class ConnectionsWindow : Window
    {
        #region Remove window close cross

        private const int GWL_STYLE = -16;
        private const int WS_SYSMENU = 0x80000;
        [DllImport("user32.dll", SetLastError = true)]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        #endregion

        public ConnectionsWindow()
        {
            this.InitializeComponent();
            this.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;

            this.Loaded += this.OnWindowLoaded;
        }

        private void SaveConnectionDataBtn_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.Save();
            this.Close();
        }

        /// <summary>
        /// Remove window close cross
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnWindowLoaded(object sender, RoutedEventArgs e)
        {
            var hwnd = new WindowInteropHelper(this).Handle;
            ConnectionsWindow.SetWindowLong(hwnd, GWL_STYLE, ConnectionsWindow.GetWindowLong(hwnd, GWL_STYLE) & ~WS_SYSMENU);
        }

        /// <summary>
        /// On PasswordBox change, save value to settings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SourceConnectionPasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.SourceConnection_Password = ((PasswordBox)sender).Password;
        }

        /// <summary>
        /// On PasswordBox change, save value to settings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DestinationConnectionPasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.DestinationConnection_Password = ((PasswordBox)sender).Password;
        }
    }
}

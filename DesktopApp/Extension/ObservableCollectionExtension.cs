﻿namespace DatabaseSubsettingTool.DesktopApp.Extension
{
    using System;
    using System.Collections.ObjectModel;

    public static class ObservableCollectionExtensions
    {
        /// <summary>
        /// Removes all elements that match condition from collection
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection"></param>
        /// <param name="condition"></param>
        public static void RemoveAll<T>(this ObservableCollection<T> collection,  Func<T, bool> condition)
        {
            for (int i = collection.Count - 1; i >= 0; i--)
            {
                if (condition(collection[i]))
                {
                    collection.RemoveAt(i);
                }
            }
        }
    }
}

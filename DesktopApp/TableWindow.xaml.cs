﻿namespace DatabaseSubsettingTool.DesktopApp
{
    using System.Windows;

    public partial class TableWindow : Window
    {
        public TableWindow()
        {
            this.InitializeComponent();
            this.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
        }
    }
}

﻿namespace DatabaseSubsettingTool.DesktopApp.Commands
{
    using System;
    using System.Windows.Input;

    internal class RelayCommand : ICommand
    {
        private readonly Action<object> action;
        private readonly Func<bool> func;

        public RelayCommand(Action<object> action, Func<bool> func)
        {
            this.action = action;
            this.func = func;
        }

        public bool CanExecute(object parameter)
        {
            return this.func == null || this.func();
        }

        public void Execute(object parameter)
        {
            this.action(parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
    }
}

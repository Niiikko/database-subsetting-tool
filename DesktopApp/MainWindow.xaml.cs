﻿namespace DatabaseSubsettingTool.DesktopApp
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Windows;

    using DatabaseSubsettingTool.Core.Enum.Workload;
    using DatabaseSubsettingTool.Core.Exception.Workload;
    using DatabaseSubsettingTool.Core.Factory.Database;
    using DatabaseSubsettingTool.Core.Factory.Workload;
    using DatabaseSubsettingTool.Core.Model.Workload;
    using DatabaseSubsettingTool.Core.Service.WorkloadLoader;
    using DatabaseSubsettingTool.Core.Service.WorkloadProcessor;
    using DatabaseSubsettingTool.Core.Service.WorkloadTester;
    using DatabaseSubsettingTool.DesktopApp.ViewModels;
    using Microsoft.Win32;

    using Table = DatabaseSubsettingTool.Core.Model.Workload.Table;

    public partial class MainWindow : Window
    {
        private readonly WorkloadLoaderFactory workloadLoaderFactory;

        public MainWindow()
        {
            this.InitializeComponent();
            this.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;

            this.DataContext = new MainWindowViewModel();

            this.workloadLoaderFactory = new WorkloadLoaderFactory();
        }

        /// <summary>
        /// Opens up a Connections window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenConnectionsWindowBtn_Click(object sender, RoutedEventArgs e)
        {
            ConnectionsWindow connectionsWindow = new ConnectionsWindow();
            connectionsWindow.ShowDialog();
            ((MainWindowViewModel)this.DataContext).ResetViewModelState();
            this.WorkloadDataGrid.Items.Refresh();

            ((MainWindowViewModel)this.DataContext).SubsettingBtnEnabled = true;
            ((MainWindowViewModel)this.DataContext).TestingBtnEnabled = true;
        }

        /// <summary>
        /// Opens up a dialog and loads workload from selected file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadWorkloadBtn_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Script files (*.sql;*.xml)|*.sql;*.xml";

            if (openFileDialog.ShowDialog() != true)
            {
                return;
            }

            IWorkloadLoader workloadLoader = this.workloadLoaderFactory.Create(openFileDialog.FileName);
            try
            {
                ((MainWindowViewModel)this.DataContext).Queries = new ObservableCollection<SqlQuery>(
                    workloadLoader.Load(openFileDialog.FileName)
                );
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                return;
            }

            ((MainWindowViewModel)this.DataContext).SubsettingBtnEnabled = true;
            ((MainWindowViewModel)this.DataContext).TestingBtnEnabled = true;
        }

        /// <summary>
        /// Closes entire application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExitBtn_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        /// <summary>
        /// Executes subsetting
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SubsetBtn_OnClick(object sender, RoutedEventArgs e)
        {
            ConnectionDataFactory connectionDataFactory = new ConnectionDataFactory();
            WorkloadProcessor workloadProcessor = new WorkloadProcessor(
                connectionDataFactory.CreateSourceFromSettings(Properties.Settings.Default),
                connectionDataFactory.CreateDestinationFromSettings(Properties.Settings.Default),
                Properties.Settings.Default.KeepReferentialIntegrity
            );

            DateTime startTime = DateTime.Now;

            try
            {
                DateTime cloningStartTime = DateTime.Now;

                workloadProcessor.Init();

                ((MainWindowViewModel)this.DataContext).CloningExecutionTime = Convert.ToInt32(
                    (DateTime.Now - cloningStartTime).TotalMilliseconds
                );
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                return;
            }

            DateTime subsettingStartTime = DateTime.Now;
            foreach (SqlQuery query in ((MainWindowViewModel)this.DataContext).Queries.Where(x => x.Valid))
            {
                try
                {
                    DateTime processingQueryStartTime = DateTime.Now;

                    workloadProcessor.ProcessOne(query);

                    query.Processed = true;
                    query.ExecutionTime = Convert.ToInt32(
                        (DateTime.Now - processingQueryStartTime).TotalMilliseconds
                    );
                }
                catch (Exception ex)
                {
                    query.Processed = false;
                    query.ErrorMessages.Add(SqlQueryErrorMessageType.Processing, ex.Message);
                }
            }

            ((MainWindowViewModel)this.DataContext).SubsettingExecutionTime = Convert.ToInt32(
                (DateTime.Now - subsettingStartTime).TotalMilliseconds
            );

            this.WorkloadDataGrid.Items.Refresh();

            try
            {
                DateTime copyingDependenciesStartTime = DateTime.Now;

                workloadProcessor.Close();

                ((MainWindowViewModel)this.DataContext).CopyingDependenciesExecutionTime = Convert.ToInt32(
                    (DateTime.Now - copyingDependenciesStartTime).TotalMilliseconds
                );
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                return;
            }

            ((MainWindowViewModel)this.DataContext).SubsettingBtnEnabled = false;
            ((MainWindowViewModel)this.DataContext).TotalExecutionTime = Convert.ToInt32(
                (DateTime.Now - startTime).TotalMilliseconds
            );
        }

        private void TestBtn_OnClick(object sender, RoutedEventArgs e)
        {
            bool unspecifiedExceptionOccured = false;

            ConnectionDataFactory connectionDataFactory = new ConnectionDataFactory();
            WorkloadTester workloadTester = new WorkloadTester(
                connectionDataFactory.CreateSourceFromSettings(Properties.Settings.Default),
                connectionDataFactory.CreateDestinationFromSettings(Properties.Settings.Default)
            );
            TableTester tableTester = new TableTester(
                connectionDataFactory.CreateSourceFromSettings(Properties.Settings.Default),
                connectionDataFactory.CreateDestinationFromSettings(Properties.Settings.Default)
            );

            foreach (SqlQuery query in ((MainWindowViewModel)this.DataContext).Queries.Where(
                x => x.Valid && x.Processed != null && (bool)x.Processed)
            )
            {
                try
                {
                    workloadTester.TestIdenticalResultCount(query);
                    query.IdenticalResultCount = true;
                }
                catch (ResultCountNotIdenticalException ex)
                {
                    query.IdenticalResultCount = false;
                    query.ErrorMessages.Add(SqlQueryErrorMessageType.IdenticalResultCount, ex.Message);
                }
                catch (Exception)
                {
                    unspecifiedExceptionOccured = true;
                }

                try
                {
                    workloadTester.TestIdenticalExecutionPlan(query);
                    query.IdenticalExecutionPlan = true;
                }
                catch (ExecutionPlanNotIdenticalException ex)
                {
                    query.IdenticalExecutionPlan = false;
                    query.ErrorMessages.Add(SqlQueryErrorMessageType.IdenticalExecutionPlan, ex.Message);
                }
                catch (Exception)
                {
                    unspecifiedExceptionOccured = true;
                }
            }

            try
            {
                workloadTester.TestReferentialIntegrityAfterSubsetting();
                ((MainWindowViewModel)this.DataContext).IsDestinationReferentianIntegrityMaintained = true;
            }
            catch (WorkloadException)
            {
                ((MainWindowViewModel)this.DataContext).IsDestinationReferentianIntegrityMaintained = false;
            }
            catch (Exception)
            {
                unspecifiedExceptionOccured = true;
            }

            try
            {
                List<Table> tables = tableTester.TestTables();
                ((MainWindowViewModel)this.DataContext).Tables = new ObservableCollection<Table>(tables);
            }
            catch (Exception)
            {
                unspecifiedExceptionOccured = true;
            }

            this.WorkloadDataGrid.Items.Refresh();

            if (unspecifiedExceptionOccured)
            {
                MessageBox.Show("An error occured while testing worklad. Some data may be incorrect. Make sure both databases exists and are available.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void TableWindowBtn_Click(object sender, RoutedEventArgs e)
        {
            TableWindow connectionsWindow = new TableWindow();
            connectionsWindow.DataContext = (MainWindowViewModel)this.DataContext;
            connectionsWindow.ShowDialog();
        }
    }
}

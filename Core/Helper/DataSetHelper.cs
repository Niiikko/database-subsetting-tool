﻿namespace DatabaseSubsettingTool.Core.Helper
{
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;

    internal class DataSetHelper
    {
        internal static DataSet Union(DataSet firstDataSet, DataSet secondDataSet)
        {
            IEnumerable<DataRow> data = firstDataSet.Tables[0].AsEnumerable().Union(
                secondDataSet.Tables[0].AsEnumerable(),
                DataRowComparer<DataRow>.Default);

            DataSet dataSet = new DataSet();
            dataSet.Tables.Add(data.CopyToDataTable());

            return dataSet;
        }

        internal static DataSet Except(DataSet firstDataSet, DataSet secondDataSet)
        {
            IEnumerable<DataRow> data = firstDataSet.Tables[0].AsEnumerable().Except(
                secondDataSet.Tables[0].AsEnumerable(),
                DataRowComparer<DataRow>.Default);

            DataSet dataSet = new DataSet();
            dataSet.Tables.Add(data.CopyToDataTable());

            return dataSet;
        }

        internal static DataSet Intersect(DataSet firstDataSet, DataSet secondDataSet)
        {
            IEnumerable<DataRow> data = firstDataSet.Tables[0].AsEnumerable().Intersect(
                secondDataSet.Tables[0].AsEnumerable(),
                DataRowComparer<DataRow>.Default);

            DataSet dataSet = new DataSet();
            dataSet.Tables.Add(data.CopyToDataTable());

            return dataSet;
        }
    }
}

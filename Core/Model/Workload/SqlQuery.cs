﻿namespace DatabaseSubsettingTool.Core.Model.Workload
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;

    using DatabaseSubsettingTool.Core.Enum.Workload;
    using DatabaseSubsettingTool.Core.Exception.Workload;
    using DatabaseSubsettingTool.Core.Validator;

    public class SqlQuery
    {
        private readonly SqlCommand sqlCommand;

        public string CommandText
        {
            get
            {
                return this.sqlCommand.CommandText;
            }
        }

        public bool Valid { get; }

        public Dictionary<SqlQueryErrorMessageType, string> ErrorMessages { get; }

        public bool? Processed { get; set; }

        public bool? IdenticalResultCount { get; set; }

        public bool? IdenticalExecutionPlan { get; set; }

        public int? ExecutionTime { get; set; }

        public SqlQuery(string commandText)
        {
            this.sqlCommand = new SqlCommand(commandText);
            this.Valid = true;
            this.ErrorMessages = new Dictionary<SqlQueryErrorMessageType, string>();
            this.Processed = null;
            this.IdenticalResultCount = null;
            this.IdenticalExecutionPlan = null;
            this.ExecutionTime = null;

            SqlQueryValidator sqlQueryValidator = new SqlQueryValidator();

            try
            {
                sqlQueryValidator.Validate(this);
            }
            catch (InvalidSqlQueryException e)
            {
                this.Valid = false;
                this.ErrorMessages.Add(SqlQueryErrorMessageType.Validation, e.Message);
            }
            catch (Exception)
            {
                this.Valid = false;
                this.ErrorMessages.Add(SqlQueryErrorMessageType.Validation, "An error occured when processing this query.");
            }
        }
    }
}

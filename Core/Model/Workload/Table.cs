﻿namespace DatabaseSubsettingTool.Core.Model.Workload
{
    public class Table
    {
        public string Name { get; }

        public int SourceRowCount { get; }

        public int DestinationRowCount { get; }

        public float PercentSubsetted { get; }

        public Table(string name, int sourceRowCount, int destinationRowCount)
        {
            this.Name = name;
            this.SourceRowCount = sourceRowCount;
            this.DestinationRowCount = destinationRowCount;

            if (sourceRowCount > 0)
            {
                this.PercentSubsetted = (float)destinationRowCount / sourceRowCount * 100;
            }
            else
            {
                this.PercentSubsetted = (float)0;
            }
        }
    }
}

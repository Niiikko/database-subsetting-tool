﻿namespace DatabaseSubsettingTool.Core.Model.Workload
{
    using System.Collections.Generic;

    public class ExecutionPlan
    {
        public string XmlText { get; set; }

        public int OperationCount { get; set; }

        public List<string> OperationNames { get; set; }

        public ExecutionPlan()
        {
            this.XmlText = string.Empty;
            this.OperationCount = 0;
            this.OperationNames = new List<string>();
        }
    }
}

﻿namespace DatabaseSubsettingTool.Core.Model.Database
{
    using DatabaseSubsettingTool.Core.Enum.Database;

    public class ConnectionData
    {
        public string Host { get; }

        public string Database { get; set; }

        public AuthMode AuthMode { get; }

        public string Username { get; }

        public string Password { get; }



        public ConnectionData(string host, string database, AuthMode authMode, string username, string password)
        {
            this.Host = host;
            this.Database = database;
            this.AuthMode = authMode;
            this.Username = username;
            this.Password = password;
        }
    }
}

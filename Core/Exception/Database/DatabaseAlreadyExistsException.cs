﻿namespace DatabaseSubsettingTool.Core.Exception.Database
{
    public class DatabaseAlreadyExistsException : DatabaseException
    {
        public DatabaseAlreadyExistsException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}

﻿namespace DatabaseSubsettingTool.Core.Exception.Database
{
    public class DatabaseNotFoundException : DatabaseException
    {
        public DatabaseNotFoundException(string message) : base(message)
        {
        }
    }
}

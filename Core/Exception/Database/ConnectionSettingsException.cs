﻿namespace DatabaseSubsettingTool.Core.Exception.Database
{
    public class ConnectionSettingsException : System.Exception
    {
        public ConnectionSettingsException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}

﻿namespace DatabaseSubsettingTool.Core.Exception.Workload
{
    public class WorkloadException : System.Exception
    {
        public WorkloadException(string message) : base(message)
        {
        }

        public WorkloadException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}

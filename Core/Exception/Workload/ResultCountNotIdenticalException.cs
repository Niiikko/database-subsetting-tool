﻿namespace DatabaseSubsettingTool.Core.Exception.Workload
{
    public class ResultCountNotIdenticalException : WorkloadException
    {
        public ResultCountNotIdenticalException(string message) : base(message)
        {
        }
    }
}

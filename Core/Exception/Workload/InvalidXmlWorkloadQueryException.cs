﻿namespace DatabaseSubsettingTool.Core.Exception.Workload
{
    public class InvalidXmlWorkloadQueryException : WorkloadException
    {
        public InvalidXmlWorkloadQueryException(string message) : base(message)
        {
        }
    }
}

﻿namespace DatabaseSubsettingTool.Core.Exception.Workload
{
    public class InvalidWorkloadFileTypeException : WorkloadException
    {
        public InvalidWorkloadFileTypeException(string message) : base(message)
        {
        }
    }
}

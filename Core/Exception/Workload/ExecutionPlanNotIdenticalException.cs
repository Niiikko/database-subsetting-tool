﻿namespace DatabaseSubsettingTool.Core.Exception.Workload
{
    public class ExecutionPlanNotIdenticalException : WorkloadException
    {
        public ExecutionPlanNotIdenticalException(string message) : base(message)
        {
        }
    }
}

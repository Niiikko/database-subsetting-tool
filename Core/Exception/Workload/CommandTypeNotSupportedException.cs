﻿namespace DatabaseSubsettingTool.Core.Exception.Workload
{
    public class QueryTypeNotSupportedException : WorkloadException
    {
        public QueryTypeNotSupportedException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}

﻿namespace DatabaseSubsettingTool.Core.Exception.Workload
{
    public class InvalidSqlQueryException : WorkloadException
    {
        public InvalidSqlQueryException(string message) : base(message)
        {
        }
    }
}

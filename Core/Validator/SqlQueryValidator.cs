﻿namespace DatabaseSubsettingTool.Core.Validator
{
    using System.Collections.Generic;
    using System.Linq;

    using DatabaseSubsettingTool.Core.Exception.Workload;
    using DatabaseSubsettingTool.Core.Model.Workload;

    using gudusoft.gsqlparser;
    using gudusoft.gsqlparser.nodes;
    using gudusoft.gsqlparser.stmt;

    internal class SqlQueryValidator
    {
        private const EDbVendor DB_VENDOR = EDbVendor.dbvmssql;
        private readonly TGSqlParser sqlparser = new TGSqlParser(DB_VENDOR);

        internal void Validate(SqlQuery query)
        {
            this.sqlparser.sqltext = query.CommandText;

            if (this.sqlparser.parse() != 0)
            {
                throw new InvalidSqlQueryException("Query has invalid syntax.");
            }

            if (this.sqlparser.sqlstatements.Count != 1)
            {
                throw new InvalidSqlQueryException("Query must contain only one statement.");
            }

            TCustomSqlStatement statement = this.sqlparser.sqlstatements.get(0);

            this.ValidateOrdinaryQuery(statement);
            this.ValidateCombinedQuery((TSelectSqlStatement)statement);
        }

        private void ValidateOrdinaryQuery(TCustomSqlStatement statement)
        {
            if (statement.sqlstatementtype != ESqlStatementType.sstselect)
            {
                throw new InvalidSqlQueryException("Query must be a select.");
            }
        }

        private void ValidateCombinedQuery(TSelectSqlStatement select)
        {
            if (select.SetOperatorType == ESetOperatorType.none)
            {
                return;
            }

            if (select.SetOperatorType == ESetOperatorType.minus)
            {
                throw new InvalidSqlQueryException("Keyword 'minus' is not supported in MSSQL.");
            }

            TSelectSqlStatement firstSelect = select.LeftStmt;
            TSelectSqlStatement secondSelect = select.RightStmt;

            this.ValidateOrdinaryQuery(firstSelect);
            this.ValidateOrdinaryQuery(secondSelect);

            if (firstSelect.SetOperatorType != ESetOperatorType.none || secondSelect.SetOperatorType != ESetOperatorType.none)
            {
                throw new InvalidSqlQueryException("Nested combined queries are not supported.");
            }

            this.ValidateQueryTargetListDoesntContainAsterisk(firstSelect);
            this.ValidateQueryTargetListDoesntContainAsterisk(secondSelect);

            if (firstSelect.ResultColumnList.Count != secondSelect.ResultColumnList.Count)
            {
                throw new InvalidSqlQueryException("Combined queries must have an equal number of expressions in their target lists.");
            }

            this.ValidateQueryIsAliased(firstSelect);
            this.ValidateQueryIsAliased(secondSelect);
            this.ValidateQueriesAreOnSameTables(firstSelect, secondSelect);
        }

        private void ValidateQueryTargetListDoesntContainAsterisk(TSelectSqlStatement select)
        {
            foreach (TResultColumn column in select.ResultColumnList)
            {
                if (column.ColumnNameOnly == "*")
                {
                    throw new InvalidSqlQueryException("Combined queries can't have asterisks in their target lists.");
                }
            }
        }

        private void ValidateQueriesAreOnSameTables(TSelectSqlStatement firstSelect, TSelectSqlStatement secondSelect)
        {
            List<string> firstSelectTargetTableNames = new List<string>();
            List<string> secondSelectTargetTableName = new List<string>();

            foreach (TTable table in firstSelect.tables)
            {
                firstSelectTargetTableNames.Add(table.Name);
            }

            foreach (TTable table in secondSelect.tables)
            {
                secondSelectTargetTableName.Add(table.Name);
            }

            if (!Enumerable.SequenceEqual(
                    firstSelectTargetTableNames.OrderBy(t => t),
                    secondSelectTargetTableName.OrderBy(t => t)))
            {
                throw new InvalidSqlQueryException("Combined queries must be executed on same tables.");
            }
        }

        private void ValidateQueryIsAliased(TSelectSqlStatement select)
        {
            List<string> tableAliases = new List<string>();

            foreach (TTable table in select.tables)
            {
                if (string.IsNullOrEmpty(table.AliasName))
                {
                    throw new InvalidSqlQueryException("Combined queries must have aliased tables.");
                }

                tableAliases.Add(table.AliasName);
            }

            foreach (TResultColumn resultColumn in select.ResultColumnList)
            {
                if (!tableAliases.Contains(resultColumn.PrefixTable))
                {
                    throw new InvalidSqlQueryException("Combined queries must have prefixed target columns with table aliases.");
                }
            }
        }
    }
}

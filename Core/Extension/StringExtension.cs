﻿namespace DatabaseSubsettingTool.Core.Extension
{
    using System;

    internal static class StringExtension
    {
        public static string ReplaceFirst(this string text, string search, string replace)
        {
            int index = text.IndexOf(search, StringComparison.Ordinal);

            if (index < 0)
            {
                return text;
            }

            return text.Substring(0, index) + replace + text.Substring(index + search.Length);
        }
    }
}

﻿namespace DatabaseSubsettingTool.Core.Enum.Workload
{
    public enum SqlQueryErrorMessageType
    {
        Validation,
        Processing,
        IdenticalResultCount,
        IdenticalExecutionPlan,
    }
}

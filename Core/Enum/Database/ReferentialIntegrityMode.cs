﻿namespace DatabaseSubsettingTool.Core.Enum.Database
{
    internal enum ReferentialIntegrityMode
    {
        Enable,
        Disable,
    }
}

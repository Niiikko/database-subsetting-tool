﻿namespace DatabaseSubsettingTool.Core.Enum.Database
{
    public enum AuthMode
    {
        WindowsAuthentication,
        SqlServerLogin,
    }
}

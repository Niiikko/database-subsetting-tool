﻿namespace DatabaseSubsettingTool.Core.Enum.Database
{
    internal enum IgnoreDuplicateKeysMode
    {
        Enable,
        Disable,
    }
}

﻿namespace DatabaseSubsettingTool.Core.Factory.Database
{
    using DatabaseSubsettingTool.Core.Enum.Database;
    using DatabaseSubsettingTool.Core.Model.Database;
    using Microsoft.SqlServer.Management.Common;
    using SMO = Microsoft.SqlServer.Management.Smo;

    public class ServerFactory
    {
        public SMO.Server Create(ConnectionData connectionData)
        {
            ServerConnection serverConnection = new ServerConnection(connectionData.Host);

            // Windows Authentication 
            if (connectionData.AuthMode == AuthMode.WindowsAuthentication)
            {
                serverConnection.LoginSecure = true;
            }

            // SQL Server Login
            if (connectionData.AuthMode == AuthMode.SqlServerLogin)
            {
                serverConnection.LoginSecure = false;
                serverConnection.Login = connectionData.Username;
                serverConnection.Password = connectionData.Password;
            }

            return new SMO.Server(serverConnection);
        }
    }
}

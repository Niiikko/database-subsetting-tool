﻿namespace DatabaseSubsettingTool.Core.Factory.Database
{
    using System;
    using System.Configuration;

    using DatabaseSubsettingTool.Core.Enum.Database;
    using DatabaseSubsettingTool.Core.Exception.Database;
    using DatabaseSubsettingTool.Core.Model.Database;

    public class ConnectionDataFactory
    {
        public ConnectionData CreateWindowsAuthenticationData(string server, string database = null)
        {
            return new ConnectionData(server, database, AuthMode.WindowsAuthentication, null, null);
        }

        public ConnectionData CreateSqlServerLoginData(
            string server,
            string username,
            string password,
            string database = null
        )
        {
            return new ConnectionData(server, database, AuthMode.SqlServerLogin, username, password);
        }

        /// <summary>
        /// Creates source connection data from settings. Must contain keys with specific names:
        ///   - SourceConnection_Host
        ///   - SourceConnection_Database
        ///   - SourceConnection_AuthMode
        ///   - SourceConnection_Login
        ///   - SourceConnection_Password
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        public ConnectionData CreateSourceFromSettings(ApplicationSettingsBase settings)
        {
            try
            {
                return new ConnectionData(
                    settings["SourceConnection_Host"].ToString(),
                    settings["SourceConnection_Database"].ToString(),
                    (dynamic)settings["SourceConnection_AuthMode"],
                    settings["SourceConnection_Login"].ToString(),
                    settings["SourceConnection_Password"].ToString()
                );
            }
            catch (Exception e)
            {
                throw new ConnectionSettingsException(
                    "Failed to create source database connection connection."
                    + "Make sure you filled in all required source connection fields in Connections window.",
                    e
                );
            }
        }

        /// <summary>
        /// Creates destination connection data from settings. Must contain keys with specific names:
        ///   - DestinationConnection_Host
        ///   - DestinationConnection_Database
        ///   - DestinationConnection_AuthMode
        ///   - DestinationConnection_Login
        ///   - DestinationConnection_Password
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        public ConnectionData CreateDestinationFromSettings(ApplicationSettingsBase settings)
        {
            try
            {
                return new ConnectionData(
                    settings["DestinationConnection_Host"].ToString(),
                    settings["DestinationConnection_Database"].ToString(),
                    (dynamic)settings["DestinationConnection_AuthMode"],
                    settings["DestinationConnection_Login"].ToString(),
                    settings["DestinationConnection_Password"].ToString()
                );
            }
            catch (Exception e)
            {
                throw new ConnectionSettingsException(
                    "Failed to create destination database connection connection."
                    + "Make sure you filled in all required destination connection fields in Connections window.",
                    e
                );
            }
        }
    }
}

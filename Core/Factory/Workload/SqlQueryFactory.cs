﻿namespace DatabaseSubsettingTool.Core.Factory.Workload
{
    using System.Linq;
    using System.Xml;

    using DatabaseSubsettingTool.Core.Exception.Workload;
    using DatabaseSubsettingTool.Core.Extension;
    using DatabaseSubsettingTool.Core.Model.Workload;

    internal class SqlQueryFactory
    {
        public SqlQuery CreateFromSql(string commandText)
        {
            return new SqlQuery(commandText);
        }

        public SqlQuery CreateFromXml(string commandText, XmlNode values = null)
        {
            if (values == null)
            {
                return new SqlQuery(commandText);
            }

            XmlNodeList parameterValues = values.SelectNodes("p");
            string parametrizedSqlQuery = commandText;

            if (parameterValues == null)
            {
                throw new InvalidXmlWorkloadQueryException(
                    "Query can't contain an empty <values> node in following query:\n\n"
                    + $"{commandText}"
                );
            }

            if (parameterValues.Count != commandText.Count(f => f == '?'))
            {
                throw new InvalidXmlWorkloadQueryException(
                    "Query parameter count doesn't equal parameter placeholders count in following query:\n\n"
                    + $"{commandText}"
                );
            }

            foreach (XmlNode parameter in parameterValues)
            {
                parametrizedSqlQuery = parametrizedSqlQuery.ReplaceFirst("?", parameter.InnerText);
            }

            return new SqlQuery(parametrizedSqlQuery);
        }
    }
}

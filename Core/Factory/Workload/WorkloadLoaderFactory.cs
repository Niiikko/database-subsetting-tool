﻿namespace DatabaseSubsettingTool.Core.Factory.Workload
{
    using System;
    using System.IO;

    using DatabaseSubsettingTool.Core.Exception.Workload;
    using DatabaseSubsettingTool.Core.Service.WorkloadLoader;

    public class WorkloadLoaderFactory
    {
        public IWorkloadLoader Create(string filePath)
        {
            string fileType = Path.GetExtension(filePath);

            if (fileType == null)
            {
                throw new InvalidWorkloadFileTypeException("Invalid workload file type. File types supported: .sql, .xml");
            }

            if (fileType.Equals(".sql", StringComparison.OrdinalIgnoreCase))
            {
                return new SqlWorkloadLoader();
            }

            if (fileType.Equals(".xml", StringComparison.OrdinalIgnoreCase))
            {
                return new XmlWorkloadLoader();
            }

            throw new InvalidWorkloadFileTypeException("Invalid workload file type. File types supported: .sql, .xml");
        }
    }
}

﻿namespace DatabaseSubsettingTool.Core.Factory.Workload
{
    using DatabaseSubsettingTool.Core.Exception.Workload;
    using DatabaseSubsettingTool.Core.Model.Workload;
    using DatabaseSubsettingTool.Core.Service.WorkloadProcessor.QueryType;
    using DatabaseSubsettingTool.Core.Service.WorkloadProcessor.QueryType.CombinedQuery;

    using gudusoft.gsqlparser;
    using gudusoft.gsqlparser.stmt;

    internal class WorkloadQueryTypeFactory
    {
        protected readonly TGSqlParser sqlparser = new TGSqlParser(EDbVendor.dbvmssql);

        public IQueryType Create(SqlQuery query)
        {
            this.sqlparser.sqltext = query.CommandText;
            this.sqlparser.parse();

            TSelectSqlStatement select = (TSelectSqlStatement)this.sqlparser.sqlstatements.get(0);

            switch (select.SetOperatorType)
            {
                case ESetOperatorType.union:
                    return new UnionQueryType();
                case ESetOperatorType.intersect:
                    return new IntersectQueryType();
                case ESetOperatorType.except:
                    return new ExceptQueryType();
                case ESetOperatorType.minus:
                    throw new InvalidSqlQueryException("Keyword 'minus' is not supported in MSSQL.");
                default:
                    return new OrdinaryQueryType();
            }
        }
    }
}

﻿namespace DatabaseSubsettingTool.Core.Service.WorkloadLoader
{
    using System.Collections.Generic;

    using DatabaseSubsettingTool.Core.Model.Workload;

    public interface IWorkloadLoader
    {
        List<SqlQuery> Load(string filePath);
    }
}

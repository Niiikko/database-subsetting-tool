﻿namespace DatabaseSubsettingTool.Core.Service.WorkloadLoader
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using DatabaseSubsettingTool.Core.Factory.Workload;
    using DatabaseSubsettingTool.Core.Model.Workload;

    public class SqlWorkloadLoader : IWorkloadLoader
    {
        private const string QUERY_DELIMITER = "--delimiter--";

        private readonly SqlQueryFactory sqlQueryFactory;

        public SqlWorkloadLoader()
        {
            this.sqlQueryFactory = new SqlQueryFactory();
        }

        public List<SqlQuery> Load(string filePath)
        {
            List<SqlQuery> sqlQueries = new List<SqlQuery>();
            string fileContents = File.ReadAllText(filePath);

            string[] queryArray = fileContents.Split(
                new string[] { QUERY_DELIMITER },
                StringSplitOptions.RemoveEmptyEntries
            );

            foreach (string commandText in queryArray)
            {
                SqlQuery sqlQuery = this.sqlQueryFactory.CreateFromSql(commandText.Trim());
                sqlQueries.Add(sqlQuery);
            }

            return sqlQueries;
        }
    }
}

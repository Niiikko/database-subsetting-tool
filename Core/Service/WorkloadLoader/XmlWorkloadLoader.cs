﻿namespace DatabaseSubsettingTool.Core.Service.WorkloadLoader
{
    using System.Collections.Generic;
    using System.Xml;

    using DatabaseSubsettingTool.Core.Exception.Workload;
    using DatabaseSubsettingTool.Core.Factory.Workload;
    using DatabaseSubsettingTool.Core.Model.Workload;

    public class XmlWorkloadLoader : IWorkloadLoader
    {
        private readonly SqlQueryFactory sqlQueryFactory;

        public XmlWorkloadLoader()
        {
            this.sqlQueryFactory = new SqlQueryFactory();
        }

        public List<SqlQuery> Load(string filePath)
        {
            List<SqlQuery> sqlQueries = new List<SqlQuery>();
            XmlDocument workload = new XmlDocument();
            workload.Load(filePath);

            XmlNodeList sqlNodes = workload.SelectNodes(".//sql");

            if (sqlNodes == null)
            {
                return sqlQueries;
            }

            foreach (XmlNode sqlNode in sqlNodes)
            {
                string commandText = sqlNode.Attributes?["text"].InnerText;
                XmlNodeList valuesList = sqlNode.SelectNodes("values");

                if (commandText == null)
                {
                    throw new InvalidXmlWorkloadQueryException("Node 'sql' must have an attribute text.");
                }

                // Query is without parameters
                if (valuesList == null || valuesList.Count == 0)
                {
                    SqlQuery sqlQuery = this.sqlQueryFactory.CreateFromXml(commandText.Trim());
                    sqlQueries.Add(sqlQuery);

                    continue;
                }

                foreach (XmlNode values in valuesList)
                {
                    SqlQuery sqlQuery = this.sqlQueryFactory.CreateFromXml(commandText.Trim(), values);
                    sqlQueries.Add(sqlQuery);
                }
            }

            return sqlQueries;
        }
    }
}

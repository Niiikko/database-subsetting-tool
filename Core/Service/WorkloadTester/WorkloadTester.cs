﻿namespace DatabaseSubsettingTool.Core.Service.WorkloadTester
{
    using System;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Xml;

    using DatabaseSubsettingTool.Core.Exception.Workload;
    using DatabaseSubsettingTool.Core.Model.Database;
    using DatabaseSubsettingTool.Core.Model.Workload;
    using DatabaseSubsettingTool.Core.Service.Database;

    using Microsoft.SqlServer.Management.Smo;

    public class WorkloadTester
    {
        private readonly DatabaseConnection sourceConnection;

        private readonly DatabaseConnection destinationConnection;

        public WorkloadTester(ConnectionData sourceConnectionData, ConnectionData destinationConnectionData)
        {
            this.sourceConnection = new DatabaseConnection(sourceConnectionData);
            this.destinationConnection = new DatabaseConnection(destinationConnectionData);
        }

        public void TestIdenticalResultCount(SqlQuery query)
        {
            if (!this.ShouldBeTested(query))
            {
                return;
            }

            this.sourceConnection.Connect();
            this.destinationConnection.Connect();

            int sourceRowCount = Convert.ToInt32(this.sourceConnection.CurrentDatabase.ExecuteWithResults(query.CommandText)
                .Tables[0]
                .Rows.Count);

            int destinationRowCount = Convert.ToInt32(this.destinationConnection.CurrentDatabase.ExecuteWithResults(query.CommandText)
                .Tables[0]
                .Rows.Count);

            this.sourceConnection.Disconnect();
            this.destinationConnection.Disconnect();

            if (sourceRowCount != destinationRowCount)
            {
                throw new ResultCountNotIdenticalException(
                    $"Query has different results on source and destination databases.\n\n"
                    + $"Source database row count: {sourceRowCount}\n"
                    + $"Destination database row count: {destinationRowCount}"
                );
            }
        }

        public void TestIdenticalExecutionPlan(SqlQuery query)
        {
            if (!this.ShouldBeTested(query))
            {
                return;
            }

            ExecutionPlan sourceExecutionPlan = this.GetExecutionPlan(this.sourceConnection, query);
            ExecutionPlan destinationExecutionPlan = this.GetExecutionPlan(this.destinationConnection, query);

            if (sourceExecutionPlan.OperationCount != destinationExecutionPlan.OperationCount)
            {
                throw new ExecutionPlanNotIdenticalException(
                    $"Query has different execution plan operation count on source and destination databases.\n\n"
                    + $"Source database operation count: {sourceExecutionPlan.OperationCount}\n"
                    + $"Destination database operation count: {destinationExecutionPlan.OperationCount}"
                );
            }

            if (!Enumerable.SequenceEqual(
                sourceExecutionPlan.OperationNames.OrderBy(t => t),
                destinationExecutionPlan.OperationNames.OrderBy(t => t)
            ))
            {
                throw new ExecutionPlanNotIdenticalException("Query executes different operations on source and destination database.");
            }
        }

        public void TestReferentialIntegrityAfterSubsetting()
        {
            try
            {
                this.destinationConnection.Connect();
                this.destinationConnection.CurrentDatabase.ExecuteNonQuery(
                    "EXEC sp_msforeachtable \"ALTER TABLE ? WITH CHECK CHECK CONSTRAINT ALL\""
                );
                this.destinationConnection.Disconnect();
            }
            catch (FailedOperationException)
            {
                throw new WorkloadException("Destination database is violating referential integrity.");
            }
        }

        private bool ShouldBeTested(SqlQuery query)
        {
            if (!query.Valid)
            {
                return false;
            }

            if (query.Processed == null || (bool)!query.Processed)
            {
                return false;
            }

            return true;
        }

        private ExecutionPlan GetExecutionPlan(DatabaseConnection databaseConnection, SqlQuery query)
        {
            string result = string.Empty;

            using (SqlConnection connection = databaseConnection.Server.ConnectionContext.SqlConnectionObject)
            {
                SqlCommand sqlCommand = new SqlCommand();

                connection.Open();
                connection.ChangeDatabase(databaseConnection.CurrentDatabase.Name);

                //Enable the statistics.
                sqlCommand.CommandText = "SET STATISTICS XML ON;";
                sqlCommand.Connection = connection;
                sqlCommand.ExecuteNonQuery();

                //Run through the query, keeping the first row first column of the last result set.
                sqlCommand.CommandText = query.CommandText;
                using (var reader = sqlCommand.ExecuteReader())
                {
                    object lastValue = null;
                    do
                    {
                        if (reader.Read())
                        {
                            lastValue = reader.GetValue(0);
                        }
                    } while (reader.NextResult());

                    if (lastValue != null)
                    {
                        result = lastValue as string;
                    }
                }
            }


            ExecutionPlan executionPlan = new ExecutionPlan();
            executionPlan.XmlText = result;

            if (string.IsNullOrEmpty(result))
            {
                return executionPlan;
            }

            XmlDocument executionPlanXml = new XmlDocument();
            executionPlanXml.LoadXml(result);

            XmlNodeList operations = executionPlanXml.SelectNodes(".//*[local-name() = 'RelOp']");

            if (operations == null)
            {
                return executionPlan;
            }

            executionPlan.OperationCount = operations.Count;

            foreach (XmlNode xmlCommand in operations)
            {
                if (xmlCommand.Attributes == null)
                {
                    break;
                }

                string operationName = xmlCommand.Attributes["LogicalOp"].InnerText;

                executionPlan.OperationNames.Add(operationName);
            }

            return executionPlan;
        }
    }
}

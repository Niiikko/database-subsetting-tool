﻿namespace DatabaseSubsettingTool.Core.Service.WorkloadTester
{
    using System.Collections.Generic;

    using DatabaseSubsettingTool.Core.Model.Database;
    using DatabaseSubsettingTool.Core.Service.Database;
    using SMO = Microsoft.SqlServer.Management.Smo;

    using Table = DatabaseSubsettingTool.Core.Model.Workload.Table;

    public class TableTester
    {
        private readonly DatabaseConnection sourceConnection;

        private readonly DatabaseConnection destinationConnection;

        public TableTester(ConnectionData sourceConnectionData, ConnectionData destinationConnectionData)
        {
            this.sourceConnection = new DatabaseConnection(sourceConnectionData);
            this.destinationConnection = new DatabaseConnection(destinationConnectionData);
        }

        public List<Table> TestTables()
        {
            List<Table> tables = new List<Table>();

            this.sourceConnection.Connect();
            this.destinationConnection.Connect();

            foreach (SMO.Table table in this.sourceConnection.CurrentDatabase.Tables)
            {
                int sourceRowCount = this.sourceConnection.Count(table.Name);
                int destinationRowCount = this.destinationConnection.Count(table.Name);

                tables.Add(new Table(table.Name, sourceRowCount, destinationRowCount));
            }

            this.sourceConnection.Disconnect();
            this.destinationConnection.Disconnect();

            return tables;
        }
    }
}

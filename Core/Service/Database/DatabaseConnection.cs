﻿namespace DatabaseSubsettingTool.Core.Service.Database
{
    using System;
    using System.Collections.Specialized;
    using System.Data;
    using System.Data.SqlClient;

    using DatabaseSubsettingTool.Core.Enum.Database;
    using DatabaseSubsettingTool.Core.Exception.Database;
    using DatabaseSubsettingTool.Core.Factory.Database;
    using DatabaseSubsettingTool.Core.Model.Database;
    using SMO = Microsoft.SqlServer.Management.Smo;

    public class DatabaseConnection
    {
        private readonly ConnectionData connectionData;
        private readonly ServerFactory serverFactory;

        private StringCollection foreignKeyScripts;

        public SMO.Server Server { get; private set; }

        public SMO.Database CurrentDatabase { get; private set; }

        public DatabaseConnection(ConnectionData connectionData)
        {
            this.connectionData = connectionData;
            this.foreignKeyScripts = new StringCollection();
            this.serverFactory = new ServerFactory();
        }

        public void Connect(bool withDatabase = true)
        {
            if (this.Server == null)
            {
                this.Server = this.serverFactory.Create(this.connectionData);
            }

            // Connection is already open
            if (this.Server.ConnectionContext.SqlConnectionObject.State == ConnectionState.Open)
            {
                return;
            }
            
            this.Server.ConnectionContext.Connect();

            // Try to connect to concrete database if it's set in connection data
            if (withDatabase && this.connectionData.Database != null)
            {
                // Database is not found on the server
                this.CurrentDatabase = this.Server.Databases[this.connectionData.Database];
                if (this.CurrentDatabase == null)
                {
                    throw new DatabaseNotFoundException($"Database '{this.connectionData.Database}' not found.");
                }
            }
        }

        public void Disconnect()
        {
            this.Server.ConnectionContext.Disconnect();
        }

        public void CreateDatabase(string name)
        {
            // Need to clear Database so it doesn't try to connect to it
            this.connectionData.Database = null;

            SMO.Database newDatabase = new SMO.Database(this.Server, name);

            try
            {
                newDatabase.Create();
            }
            catch (SMO.FailedOperationException e)
            {
                throw new DatabaseAlreadyExistsException(
                    $"Database '{name}' already exists. Choose a different database name.",
                    e);
            }

            this.CurrentDatabase = newDatabase;
            this.connectionData.Database = name;
        }

        public void DropDatabase(string name)
        {
            SMO.Database database = this.Server.Databases[name];

            if (database == null)
            {
                throw new DatabaseNotFoundException($"Database '{name}' not found.");
            }

            database.Drop();
        }

        internal void DropForeignKeys()
        {
            foreach (SMO.Table table in this.CurrentDatabase.Tables)
            {
                for (int i = table.ForeignKeys.Count - 1; i >= 0; i--)
                {
                    SMO.ForeignKey foreignKey = table.ForeignKeys[i];

                    foreach (string script in foreignKey.Script())
                    {
                        this.foreignKeyScripts.Add(script);
                    }
                   
                    foreignKey.Drop();
                }
            }
        }

        internal void RecreateForeignKeys()
        {
            foreach (string script in this.foreignKeyScripts)
            {
                this.CurrentDatabase.ExecuteNonQuery(script);
            }

            this.foreignKeyScripts.Clear();
            this.CurrentDatabase.Refresh();
        }

        internal void IgnoreDuplicateKeys(IgnoreDuplicateKeysMode insertDuplicateKeysMode)
        {
            bool isEnabled = (insertDuplicateKeysMode == IgnoreDuplicateKeysMode.Enable);
            string onOrOff = isEnabled ? "ON" : "OFF";

            foreach (SMO.Table table in this.CurrentDatabase.Tables)
            {
                string alterQuery = $"ALTER TABLE[{table.Name}] REBUILD WITH(IGNORE_DUP_KEY = {onOrOff})";

                this.CurrentDatabase.ExecuteNonQuery(alterQuery);
            }
        }

        internal int BulkInsert(DataSet data, string tableName)
        {
            using (SqlConnection connection = this.Server.ConnectionContext.SqlConnectionObject)
            {
                SqlBulkCopy bulkCopy =
                    new SqlBulkCopy
                    (
                        connection,
                        SqlBulkCopyOptions.TableLock
                        | SqlBulkCopyOptions.FireTriggers
                        | SqlBulkCopyOptions.UseInternalTransaction
                        | SqlBulkCopyOptions.KeepIdentity,
                        null
                    );

                bulkCopy.DestinationTableName = tableName;

                int beforeInsertRowCount = this.Count(tableName);

                connection.ChangeDatabase(this.CurrentDatabase.Name);
                bulkCopy.WriteToServer(data.Tables[0]);

                int afterInsertRowCount = this.Count(tableName);

                return afterInsertRowCount - beforeInsertRowCount;
            }
        }

        internal int Count(string table)
        {
            string select = @"SELECT ddps.row_count
                FROM sys.indexes AS i
                INNER JOIN sys.objects AS o ON i.OBJECT_ID = o.OBJECT_ID
                INNER JOIN sys.dm_db_partition_stats AS ddps ON i.OBJECT_ID = ddps.OBJECT_ID
                AND i.index_id = ddps.index_id
                WHERE i.index_id < 2
                AND o.is_ms_shipped = 0
                AND o.name = '" + table + "' ORDER BY o.NAME";

            DataSet result = this.CurrentDatabase.ExecuteWithResults(select);

            return Convert.ToInt32(result.Tables[0].Rows[0].ItemArray[0]);
        }
    }
}
﻿namespace DatabaseSubsettingTool.Core.Service.Database
{
    using System;
    using System.IO;

    using DatabaseSubsettingTool.Core.Service.FileAccess;

    using SMO = Microsoft.SqlServer.Management.Smo;

    internal class DatabaseCloner
    {
        private readonly CommonApplicationData commonApplicationData;

        public DatabaseCloner()
        {
            this.commonApplicationData = new CommonApplicationData("nikko", "database-subsetting-tool");
        }

        internal void Clone(DatabaseConnection sourceConnection, DatabaseConnection destinationConnection)
        {
            string backupFileName = $"{this.commonApplicationData.ApplicationFolderPath}\\backup-{DateTimeOffset.UtcNow.ToUnixTimeSeconds()}.bak";

            sourceConnection.Connect();

            SMO.Backup backup = new SMO.Backup();
            backup.Action = SMO.BackupActionType.Database;
            backup.Database = sourceConnection.CurrentDatabase.Name;
            backup.Devices.AddDevice(backupFileName, SMO.DeviceType.File);
            backup.Initialize = false;

            backup.SqlBackup(sourceConnection.Server);

            sourceConnection.Disconnect();
            destinationConnection.Connect();

            string sql = $"USE master; RESTORE DATABASE [{destinationConnection.CurrentDatabase.Name}]"
                         + $" FROM DISK = '{backupFileName}' WITH RECOVERY,"
                         + $" MOVE '{sourceConnection.CurrentDatabase.FileGroups[0].Files[0].Name}'"
                         + $" TO '{destinationConnection.CurrentDatabase.PrimaryFilePath}\\{destinationConnection.CurrentDatabase.Name}.mdf',"
                         + $" MOVE '{sourceConnection.CurrentDatabase.LogFiles[0].Name}'"
                         + $" TO '{destinationConnection.CurrentDatabase.PrimaryFilePath}\\{destinationConnection.CurrentDatabase.Name}_log.ldf', REPLACE";

            destinationConnection.CurrentDatabase.ExecuteNonQuery(sql);
            
            destinationConnection.DropForeignKeys();
            foreach (SMO.Table table in destinationConnection.CurrentDatabase.Tables)
            {
                table.TruncateData();
            }
            destinationConnection.RecreateForeignKeys();

            foreach (SMO.Table table in destinationConnection.CurrentDatabase.Tables)
            {
                table.ForeignKeys.Refresh(true);
            }

            destinationConnection.Disconnect();

            File.Delete(backupFileName);
        }
    }
}

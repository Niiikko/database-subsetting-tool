﻿namespace DatabaseSubsettingTool.Core.Service.WorkloadProcessor.QueryType
{
    using DatabaseSubsettingTool.Core.Model.Workload;
    using DatabaseSubsettingTool.Core.Service.Database;

    internal interface IQueryType
    {
        void Subset(DatabaseConnection sourceConnection, DatabaseConnection destinationConnection, SqlQuery query);
    }
}

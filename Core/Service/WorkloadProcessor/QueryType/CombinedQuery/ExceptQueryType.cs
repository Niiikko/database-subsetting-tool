﻿namespace DatabaseSubsettingTool.Core.Service.WorkloadProcessor.QueryType.CombinedQuery
{
    using System.Data;

    using DatabaseSubsettingTool.Core.Helper;
    using DatabaseSubsettingTool.Core.Model.Workload;
    using DatabaseSubsettingTool.Core.Service.Database;

    using gudusoft.gsqlparser.nodes;
    using gudusoft.gsqlparser.stmt;

    internal class ExceptQueryType : AbstractCombinedQueryType
    {
        public override void Subset(DatabaseConnection sourceConnection, DatabaseConnection destinationConnection, SqlQuery query)
        {
            this.sqlparser.sqltext = query.CommandText;
            this.sqlparser.parse();
            TSelectSqlStatement select = (TSelectSqlStatement)this.sqlparser.sqlstatements.get(0);

            TSelectSqlStatement firstSelect = select.LeftStmt;
            TSelectSqlStatement secondSelect = select.RightStmt;

            foreach (TTable table in firstSelect.tables)
            {
                sourceConnection.Connect();
                destinationConnection.Connect();

                TTable correspodingSecondSelectTable = this.GetCorrespondingTableInSecondSelect(secondSelect, table);

                SqlQuery firstAtomicQuery = this.ReplaceSelectColumns(firstSelect, table);
                SqlQuery secondAtomicQuery = this.ReplaceSelectColumns(secondSelect, correspodingSecondSelectTable);

                DataSet firstSelectData = sourceConnection.CurrentDatabase.ExecuteWithResults(firstAtomicQuery.CommandText);
                DataSet secondSelectData = sourceConnection.CurrentDatabase.ExecuteWithResults(secondAtomicQuery.CommandText);

                destinationConnection.BulkInsert(DataSetHelper.Except(firstSelectData, secondSelectData), table.Name);

                sourceConnection.Disconnect();
                destinationConnection.Disconnect();
            }
        }
    }
}

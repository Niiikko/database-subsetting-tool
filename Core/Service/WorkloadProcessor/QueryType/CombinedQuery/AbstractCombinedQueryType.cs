﻿namespace DatabaseSubsettingTool.Core.Service.WorkloadProcessor.QueryType.CombinedQuery
{
    using gudusoft.gsqlparser.nodes;
    using gudusoft.gsqlparser.stmt;

    internal abstract class AbstractCombinedQueryType : AbstractQueryType
    {
        protected TTable GetCorrespondingTableInSecondSelect(TSelectSqlStatement secondSelect, TTable table)
        {
            foreach (TTable secondSelectTable in secondSelect.tables)
            {
                if (secondSelectTable.Name == table.Name)
                {
                    return secondSelectTable;
                }
            }

            return null;
        }
    }
}

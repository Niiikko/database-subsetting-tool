﻿
namespace DatabaseSubsettingTool.Core.Service.WorkloadProcessor.QueryType
{
    using DatabaseSubsettingTool.Core.Model.Workload;
    using DatabaseSubsettingTool.Core.Service.Database;

    using gudusoft.gsqlparser;
    using gudusoft.gsqlparser.nodes;
    using gudusoft.gsqlparser.stmt;

    internal abstract class AbstractQueryType : IQueryType
    {
        protected readonly TGSqlParser sqlparser = new TGSqlParser(EDbVendor.dbvmssql);

        public abstract void Subset(DatabaseConnection sourceConnection, DatabaseConnection destinationConnection, SqlQuery query);

        protected SqlQuery ReplaceSelectColumns(TSelectSqlStatement select, TTable table)
        {
            TResultColumn column = select.ResultColumnList.getResultColumn(0);
            string columnPrefix = table.Name;

            if (!string.IsNullOrEmpty(table.AliasName))
            {
                columnPrefix = table.AliasName;
            }


            if (column.FieldAttr == null)
            {
                column.String = $"{columnPrefix}.*";
            }
            else
            {
                if (column.FieldAttr.ObjectToken == null)
                {
                    column.FieldAttr.ObjectToken = new TSourceToken(columnPrefix);
                }

                column.FieldAttr.ObjectToken.String = columnPrefix;

                if (column.FieldAttr.PartToken != null)
                {
                    column.FieldAttr.PartToken.String = "*";
                }

                if (column.FieldAttr.ColumnToken != null)
                {
                    column.FieldAttr.ColumnToken.String = "*";
                }
            }

            select.ResultColumnList = new TResultColumnList();
            select.ResultColumnList.addResultColumn(column);

            string script = select.ToScript();

            return new SqlQuery(script);
        }
    }
}

﻿namespace DatabaseSubsettingTool.Core.Service.WorkloadProcessor.QueryType
{
    using System.Data;

    using DatabaseSubsettingTool.Core.Model.Workload;
    using DatabaseSubsettingTool.Core.Service.Database;

    using gudusoft.gsqlparser.nodes;
    using gudusoft.gsqlparser.stmt;

    internal class OrdinaryQueryType : AbstractQueryType
    {
        public override void Subset(DatabaseConnection sourceConnection, DatabaseConnection destinationConnection, SqlQuery query)
        {
            this.sqlparser.sqltext = query.CommandText;
            this.sqlparser.parse();
            TSelectSqlStatement select = (TSelectSqlStatement)this.sqlparser.sqlstatements.get(0);

            foreach (TTable table in select.tables)
            {
                SqlQuery atomicQuery = this.ReplaceSelectColumns(select, table);

                sourceConnection.Connect();
                destinationConnection.Connect();

                DataSet data = sourceConnection.CurrentDatabase.ExecuteWithResults(atomicQuery.CommandText);
                destinationConnection.BulkInsert(data, table.Name);

                sourceConnection.Disconnect();
                destinationConnection.Disconnect();
            }
        }
    }
}

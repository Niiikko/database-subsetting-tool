﻿namespace DatabaseSubsettingTool.Core.Service.WorkloadProcessor
{
    using System;
    using System.Collections.Generic;
    using System.Data;

    using DatabaseSubsettingTool.Core.Enum.Database;
    using DatabaseSubsettingTool.Core.Exception.Workload;
    using DatabaseSubsettingTool.Core.Factory.Workload;
    using DatabaseSubsettingTool.Core.Model.Database;
    using DatabaseSubsettingTool.Core.Model.Workload;
    using DatabaseSubsettingTool.Core.Service.Database;
    using DatabaseSubsettingTool.Core.Service.WorkloadProcessor.QueryType;

    using Microsoft.SqlServer.Management.Common;

    using SMO = Microsoft.SqlServer.Management.Smo;

    public class WorkloadProcessor
    {
        private readonly ConnectionData sourceConnectionData;

        private readonly ConnectionData destinationConnectionData;

        private readonly bool keepReferentialIntegrity;

        private readonly WorkloadQueryTypeFactory workloadQueryTypeFactory;

        private readonly DatabaseCloner databaseCloner;

        private DatabaseConnection sourceConnection;

        private DatabaseConnection destinationConnection;

        public WorkloadProcessor(ConnectionData sourceConnectionData, ConnectionData destinationConnectionData, bool keepReferentialIntegrity = true)
        {
            this.sourceConnectionData = sourceConnectionData;
            this.destinationConnectionData = destinationConnectionData;
            this.keepReferentialIntegrity = keepReferentialIntegrity;

            this.workloadQueryTypeFactory = new WorkloadQueryTypeFactory();
            this.databaseCloner = new DatabaseCloner();
        }

        public void Init()
        {
            this.sourceConnection = new DatabaseConnection(this.sourceConnectionData);
            this.destinationConnection = new DatabaseConnection(this.destinationConnectionData);

            if (this.destinationConnection.CurrentDatabase == null)
            {
                this.destinationConnection.Connect(false);
                this.destinationConnection.CreateDatabase(this.destinationConnectionData.Database);
                this.destinationConnection.Disconnect();
            }

            this.databaseCloner.Clone(this.sourceConnection, this.destinationConnection);

            this.destinationConnection.IgnoreDuplicateKeys(IgnoreDuplicateKeysMode.Enable);
        }

        public void Close()
        {
            if (this.keepReferentialIntegrity)
            {
                this.CopyDependencies();
            }

            this.destinationConnection.IgnoreDuplicateKeys(IgnoreDuplicateKeysMode.Disable);
        }

        public void ProcessOne(SqlQuery query)
        {
            if (!query.Valid)
            {
                return;
            }

            try
            {
                // Try to execute query to catch errors from mssql
                this.sourceConnection.CurrentDatabase.ExecuteWithResults(query.CommandText);

                IQueryType queryType = this.workloadQueryTypeFactory.Create(query);
                queryType.Subset(this.sourceConnection, this.destinationConnection, query);
            }
            catch (Exception e)
            {
                string errorMessage = "Query couldn't be processed.";

                if (e.InnerException is SMO.FailedOperationException || e.InnerException is ExecutionFailureException)
                {
                    errorMessage += $"\n\n{e.InnerException.InnerException?.Message}";
                }

                throw new WorkloadException(errorMessage, e);
            }
        }

        private void CopyDependencies()
        {
            this.sourceConnection.Connect();
            this.destinationConnection.Connect();

            Queue<SMO.Table> tablesQueue = new Queue<SMO.Table>();
            SMO.TableCollection databaseTables = this.destinationConnection.CurrentDatabase.Tables;

            foreach (SMO.Table table in databaseTables)
            {
                tablesQueue.Enqueue(table);
            }

            while (tablesQueue.Count > 0)
            {
                SMO.Table table = tablesQueue.Dequeue();

                foreach (SMO.ForeignKey foreignKey in table.ForeignKeys)
                {
                    foreach (SMO.ForeignKeyColumn foreignKeyColumn in foreignKey.Columns)
                    {
                        this.destinationConnection.Connect();

                        // Select referenced IDs from table in destination database
                        string referencedIdsSelect = $"SELECT DISTINCT {foreignKeyColumn.Name}"
                                                     + $" FROM {table.Name}";
                        DataSet tableIds = this.destinationConnection.CurrentDatabase.ExecuteWithResults(referencedIdsSelect);
                        DataRowCollection tableIdsRows = tableIds.Tables[0].Rows;

                        // Already inserted IDs in destination database
                        string alreadyInsertedIdsSelect = $"SELECT DISTINCT {foreignKeyColumn.ReferencedColumn}"
                                                          + $" FROM {foreignKey.ReferencedTable}";
                        DataSet alreadyInsertedDataIds = this.destinationConnection.CurrentDatabase.ExecuteWithResults(alreadyInsertedIdsSelect);
                        DataRowCollection alreadyInsertedDataIdsRows = alreadyInsertedDataIds.Tables[0].Rows;

                        this.destinationConnection.Disconnect();

                        HashSet<string> idsHashSet = new HashSet<string>();
                        HashSet<string> insertedIdsHashSet = new HashSet<string>();

                        // Add already inserted IDs to hash set
                        foreach (DataRow row in alreadyInsertedDataIdsRows)
                        {
                            // We need to know which type ID is, strings must be in '' and integers can't be in ''
                            var id = row.ItemArray[0];

                            if (id is string)
                            {
                                id = $"'{id}'";
                            }

                            insertedIdsHashSet.Add(id.ToString());
                        }

                        // Add only IDs that are not inserted yet to hash set
                        foreach (DataRow row in tableIdsRows)
                        {
                            // In case of nullable foreign key
                            if (row.IsNull(foreignKeyColumn.Name))
                            {
                                continue;
                            }

                            // We need to know which type ID is, strings must be in '' and integers can't be in ''
                            var id = row.ItemArray[0];

                            if (id is string)
                            {
                                id = $"'{id}'";
                            }

                            if (!insertedIdsHashSet.Contains(id.ToString()))
                            {
                                idsHashSet.Add(id.ToString());
                            }
                        }

                        // No IDs to insert
                        if (idsHashSet.Count <= 0)
                        {
                            continue;
                        }

                        this.sourceConnection.Connect();
                        this.destinationConnection.Connect();

                        // Select rows from referenced table in source database
                        string referencedRowsSelect = $"SELECT * FROM {foreignKey.ReferencedTable}"
                                                      + $" WHERE {foreignKeyColumn.ReferencedColumn}"
                                                      + $" IN ({string.Join(",", idsHashSet)})";
                        DataSet referencedData = this.sourceConnection.CurrentDatabase.ExecuteWithResults(referencedRowsSelect);

                        // Insert rows into destination database
                        this.destinationConnection.BulkInsert(referencedData, foreignKey.ReferencedTable);

                        this.sourceConnection.Disconnect();
                        this.destinationConnection.Disconnect();

                        // Add referenced table to queue if not already there
                        SMO.Table referencedTable = this.destinationConnection.CurrentDatabase.Tables[foreignKey.ReferencedTable];
                        if (!tablesQueue.Contains(referencedTable))
                        {
                            tablesQueue.Enqueue(referencedTable);
                        }
                    }
                }
            }
        }
    }
}
